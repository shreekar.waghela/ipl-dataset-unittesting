### IPL Data Unit Testing

In this project we perform unit testing for all parts of the problem using unittesting module of python.

### Part-1:
This part plots the total number of matches played in all years.

### Part-2:
This part plots a stacked bar chart of matches won by each team in all the years.

### Part-3:
This part plots the extra runs conceded by each team in year 2016.

### Part-4:
This part plots the top economical bowlers in the year 2015.

### Part-5:
This part plots the highest wicket takers in IPL history.