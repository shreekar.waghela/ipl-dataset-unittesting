import unittest
from part2 import extract


class TestPart2(unittest.TestCase):

    def test_part2(self):
        self.assertEqual(extract("mock_matches.csv"),
                         {'Rajasthan Royals': {'2015': 1},
                          'Chennai Super Kings': {'2015': 1},
                          'Delhi Daredevils': {'2015': 1, '2016': 1},
                          'Sunrisers Hyderabad': {'2016': 1},
                          'Kolkata Knight Riders': {'2016': 1}})

        self.assertEqual(extract('test.csv'), 'File you entered not found')
        self.assertEqual(extract('mock_deliveries.csv'), 'Wrong File')


if __name__ == '__main__':
    unittest.main()
