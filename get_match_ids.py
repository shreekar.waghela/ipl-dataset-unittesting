import csv


def ids_of_matches_played(year):
    try:
        match_ids = []

        if type(year) != int and type(year) != str:
            raise TypeError
        year = str(year)
        season = ['2015', '2016']
        if not (year.isnumeric()) or year not in season:
            raise ValueError
        
        with open("mock_matches.csv") as matches_file:
            match_stats_reader = csv.DictReader(matches_file)
            for match in match_stats_reader:
                if match['season'] == year:
                    match_ids.append(match['id'])
        return match_ids
    except ValueError:
        return 'Enter correct year'
    except TypeError:
        return 'Enter integer or string value'
