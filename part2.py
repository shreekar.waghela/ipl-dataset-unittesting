import csv


def extract(file_path):
    """Extracts data from matches csv for plotting"""
    try:
        with open(file_path) as match_file:
            match_stats_reader = csv.DictReader(match_file)

            match_wins = {}
            for match in match_stats_reader:
                winner = match['winner']
                season = match['season']
                if winner in match_wins:
                    if season in match_wins[winner]:
                        match_wins[winner][season] += 1
                    else:
                        match_wins[winner][season] = 1
                else:
                    match_wins[winner] = {season: 1}
        return match_wins
    except FileNotFoundError:
        return 'File you entered not found'
    except KeyError:
        return 'Wrong File'
