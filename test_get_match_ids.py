import unittest
from get_match_ids import ids_of_matches_played


class TestGetMatchIds(unittest.TestCase):

    def test_get_match_ids(self):
        self.assertEqual(ids_of_matches_played('2015'), ['528', '529', '530'])
        self.assertEqual(ids_of_matches_played('2016'), ['587', '588', '589'])
        self.assertEqual(ids_of_matches_played(2015), ['528', '529', '530'])
        self.assertEqual(ids_of_matches_played(2016), ['587', '588', '589'])
        self.assertEqual(ids_of_matches_played(
            'Two Thousand Fifteen'), 'Enter correct year')
        self.assertEqual(ids_of_matches_played([2016]), 'Enter integer or string value')
        self.assertEqual(ids_of_matches_played('2000'), 'Enter correct year')


if __name__ == '__main__':
    unittest.main()
