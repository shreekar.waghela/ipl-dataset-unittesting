import csv
from get_match_ids import ids_of_matches_played


def extract(match_ids_season, file_path):
    """Extracts data from deliveries csv for calculating"""
    try:
        with open(file_path) as deliveries_file:
            delivery_stats_reader = csv.DictReader(deliveries_file)

            bowler_stats = {}
            for delivery in delivery_stats_reader:
                bowler = delivery['bowler']
                wide_runs = int(delivery['wide_runs'])
                noball_runs = int(delivery['noball_runs'])
                batsman_runs = int(delivery['batsman_runs'])
                runs_per_ball = wide_runs + noball_runs + batsman_runs

                if delivery['match_id'] in match_ids_season:
                    if delivery['is_super_over'] == '0':
                        if bowler in bowler_stats:
                            bowler_stats[bowler]['runs_conceded'] += runs_per_ball
                            bowler_stats[bowler]['balls'] += 1
                        else:
                            bowler_stats[bowler] = {
                                'runs_conceded': runs_per_ball, 'balls': 1}
                        if wide_runs != 0 or noball_runs != 0:
                            bowler_stats[bowler]['balls'] -= 1

        bowler_economy = {}
        for bowler in bowler_stats:
            economy = (bowler_stats[bowler]['runs_conceded']
                       )/(bowler_stats[bowler]['balls']/6)
            bowler_economy[bowler] = round(economy, 2)
        return bowler_economy
    except FileNotFoundError:
        return 'File you entered not found'
    except KeyError:
        return 'Wrong File'
