import unittest
from part3 import extract
from get_match_ids import ids_of_matches_played


class TestPart3(unittest.TestCase):

    def test_part3(self):
        self.assertEqual(extract(ids_of_matches_played('2016'), "mock_deliveries.csv"),
                         {'Sunrisers Hyderabad': 0, 'Royal Challengers Bangalore': 1, 'Delhi Daredevils': 0,
                          'Mumbai Indians': 0, 'Kings XI Punjab': 1, 'Kolkata Knight Riders': 0})

        self.assertEqual(extract(ids_of_matches_played('2015'), 'mock_deliveries.csv'),
                         {'Delhi Daredevils': 1, 'Mumbai Indians': 0, 'Kings XI Punjab': 0, 'Rajasthan Royals': 0, 'Sunrisers Hyderabad': 0, 'Chennai Super Kings': 0})

        self.assertEqual(extract(ids_of_matches_played(
            '2016'), "mock_matches.csv"), 'Wrong File')
        self.assertEqual(extract(ids_of_matches_played(
            '2016'), "test.csv"), 'File you entered not found')
        self.assertEqual(extract(ids_of_matches_played(
            '2015'), "mock_matches.csv"), 'Wrong File')
        self.assertEqual(extract(ids_of_matches_played(
            '2015'), "test.csv"), 'File you entered not found')



if __name__ == "__main__":
    unittest.main()
