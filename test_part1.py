import unittest
from part1 import extract


class TestPart1(unittest.TestCase):

    def test_part1(self):
        self.assertEqual(extract("mock_matches.csv"),
                         {'2015': 3, '2016': 3})

        self.assertEqual(extract('test.csv'), 'File you entered not found')
        self.assertEqual(extract('mock_deliveries.csv'), 'Wrong File')


if __name__ == "__main__":
    unittest.main()
