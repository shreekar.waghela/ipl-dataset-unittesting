import csv
from get_match_ids import ids_of_matches_played


def extract(match_id_season, file_path):
    """Extracts data from matches and deliveries csv for plotting"""
    try:
        with open(file_path) as deliveries_file:
            delivery_stats_reader = csv.DictReader(deliveries_file)

            runs_conceded = {}
            for delivery in delivery_stats_reader:
                match_id = delivery['match_id']
                bowling_team = delivery['bowling_team']
                extra_runs = int(delivery['extra_runs'])
                if match_id in match_id_season:
                    if bowling_team in runs_conceded:
                        runs_conceded[bowling_team] += extra_runs
                    else:
                        runs_conceded[bowling_team] = extra_runs
        return runs_conceded
    except FileNotFoundError:
        return 'File you entered not found'
    except KeyError:
        return 'Wrong File'
