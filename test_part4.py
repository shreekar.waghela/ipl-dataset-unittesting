import unittest
from part4 import extract
from get_match_ids import ids_of_matches_played


class TestPart4(unittest.TestCase):

    def test_part4(self):
        self.assertEqual(extract(ids_of_matches_played('2015'), "mock_deliveries.csv"),
                         {'AD Mathews': 6.0, 'R Dhawan': 6.0, 'DS Kulkarni': 4.0, 'P Kumar': 10.0,
                          'MM Sharma': 6.0, 'J Suchith': 20.0})

        self.assertEqual(extract(ids_of_matches_played('2016'), "mock_deliveries.csv"),
                         {'P Negi': 10.0, 'SR Watson': 15.0, 'BB Sran': 6.0, 'TG Southee': 14.0,
                          'M Morkel': 2.0, 'MM Sharma': 15.0})

        self.assertEqual(extract(ids_of_matches_played(
            '2015'), "mock_matches.csv"), 'Wrong File')
        self.assertEqual(extract(ids_of_matches_played(
            '2015'), "test.csv"), 'File you entered not found')
        self.assertEqual(extract(ids_of_matches_played(
            '2016'), "mock_matches.csv"), 'Wrong File')
        self.assertEqual(extract(ids_of_matches_played(
            '2016'), "test.csv"), 'File you entered not found')



if __name__ == "__main__":
    unittest.main()
