import csv


def extract(file_path):
    """ Extracts data from csv file and obtains data for plotting"""
    try:
        with open(file_path) as matches_file:
            match_stats_reader = csv.DictReader(matches_file)

            matches_played = {}
            for match in match_stats_reader:
                if match['season'] in matches_played:
                    matches_played[match['season']] += 1
                else:
                    matches_played[match['season']] = 1
        return matches_played
    except FileNotFoundError:
        return "File you entered not found"
    except KeyError:
        return 'Wrong File'
